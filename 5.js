// Write a javascript function taking a token as first argument. It will call a Restful on the URL `/api/account` and returns a promise with the retrieved data. The response paylaod will be in JSON. The function must :

// * Provider a bearer token in the `Authorization` header
// * Wait for the response and process it
// * Throw an exception if the response is invalid
// * Use the Fetch API

async function getAccount (token) {
    const apiURL = '/api/account'
    try {
        const res = await fetch(apiURL, {
            method: 'GET',
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })
        if (!response.ok) {
            throw new Error("Something went wrong")
        }
        const data = await response.json()
        return data
    }
    
    catch (err) {
        throw new Error(`Fetch failed! ${err.message || 'Something went wrong'}`)
    }
}