// Write a javascript function `getFirstUniq` taking a list of integer as first argument and returning the first unique integer from the given list, would return `null` if there's none. Please provide the most optimized implementation you can think of (meaning with the lesser iterations)
function getFirstUniq(integers) {
    const intTimes = new Map()
    for (let int in integers) {
        const value = integers[int]
        if (intTimes.has(value)) {
            intTimes.set(value, intTimes.get(value) + 1)
        }
        else {
            intTimes.set(value, 1)
        }
    }
    
    for (let int in integers) {
        const value = integers[int]
        if (intTimes.get(value) === 1) {
            return integers[int]
        }
    }
    
    return null
}