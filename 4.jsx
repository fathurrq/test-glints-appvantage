// Write a React component named `MyCounter`

// It takes one optional property named `defaultOffset` of type integer with default value 0.

// It must render two elements : 
// * the current Unix timestamp (in seconds) minus the current offset
// * a button on which the text would be `add offset` suffixed with the current offset surrounded parenthesis. With an offset of two the text would become `add offset (2)`
// * when clicking on the button the offset will increase by one

// You may either use hooks or classes.

import React, { useState, useEffect } from 'react';

const MyCounter = ({ defaultOffset = 0 }) => {
  const [offset, setOffset] = useState(defaultOffset);
  const [timestamp, setTimestamp] = useState(Math.floor(Date.now() / 1000));

  useEffect(() => {
    const timer = setInterval(() => {
      setTimestamp(Math.floor(Date.now() / 1000));
    }, 1000);

    return () => clearInterval(timer);
  }, []);

  const handleClick = () => {
    setOffset(prevOffset => prevOffset + 1);
  };

  return (
    <div>
      <div>Current Unix timestamp minus offset: {timestamp - offset}</div>
      <button onClick={handleClick}>
        add offset ({offset})
      </button>
    </div>
  );
};

export default MyCounter;