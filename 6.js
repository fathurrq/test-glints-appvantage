// What is Exception Handling in JavaScript. Can you give an example of exception handling in Javascript ?

// Exception handling in JavaScript is a way to manage errors that may occur during the execution of your code. It allows you to catch and handle errors gracefully, rather than letting the program crash. 

// Example :

try {
    let jsonString = '{"name": "Fathur", "age": 25}';
    let user = JSON.parse(jsonString);
    
    console.log(user.name);
    console.log(user.age);  
  
    // simulate the error by making the JSON in invalid format
    let invalidJsonString = '{"name": "Qarnain", "age": 28';
    let invalidUser = JSON.parse(invalidJsonString); // This will throw an error
  
  } catch (error) {
    // code to handle error
    console.log("An error occurred while parsing JSON:", error.message);
  } finally {
    console.log("Parsing attempt finished.");
  }