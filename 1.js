// Write a javascript function `doFactorial` taking a number as first argument and returns the factorial of it ; such as doFactorial(4) returns 24 (computed by doing 4 * 3 * 2 * 1).

function doFactorial(num) {
    if (num === 0 || num === 1) return 1
    return num * doFactorial(num - 1)
    
}
